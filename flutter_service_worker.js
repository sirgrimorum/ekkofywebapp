'use strict';
const MANIFEST = 'flutter-app-manifest';
const TEMP = 'flutter-temp-cache';
const CACHE_NAME = 'flutter-app-cache';
const RESOURCES = {
  "assets/AssetManifest.json": "dc9ebd06f15135abb36a445dd2a725de",
"assets/assets/fonts/EkkofyFont.ttf": "4f04f98c4ab908df9b21dfe45a7672a7",
"assets/assets/fonts/EkkofyIcons.ttf": "0fd2d3d444ee80708587e4ec5efed6a0",
"assets/assets/fonts/Nunito-Black.ttf": "ece7e9a31268e78a2ea7d262231e8a25",
"assets/assets/fonts/Nunito-Bold.ttf": "6cfc350084372090228fc8630b3c7eb1",
"assets/assets/fonts/Nunito-Medium.ttf": "26a1b6a894383eba0c5139e8b7174677",
"assets/assets/fonts/Nunito-Regular.ttf": "6198e4056babec12dec402d68fb002f7",
"assets/assets/fonts/Roboto-Black.ttf": "ec4c9962ba54eb91787aa93d361c10a8",
"assets/assets/fonts/Roboto-Bold.ttf": "ee7b96fa85d8fdb8c126409326ac2d2b",
"assets/assets/fonts/Roboto-Medium.ttf": "d08840599e05db7345652d3d417574a9",
"assets/assets/fonts/Roboto-Regular.ttf": "3e1af3ef546b9e6ecef9f3ba197bf7d2",
"assets/assets/icons/2x/cat_todos.png": "ef3821795f6b20c792b0b8d580b8466c",
"assets/assets/icons/2x/celular.png": "e59ca8f1cdae43e63a5772773dc4495c",
"assets/assets/icons/2x/mail.png": "78ff88b77f4953b97790d75b62e10f3c",
"assets/assets/icons/3x/celular.png": "bebd0ec45c69d73a7372ffdbb742c66d",
"assets/assets/icons/3x/mail.png": "56bdac7fa1aa81cea1795fc008bf63d7",
"assets/assets/icons/cat_todos.png": "1c680fc58b8d1f3a01ea72a366cc6ead",
"assets/assets/icons/celular.png": "09a01a054e4a2e385b44a9e74080fd21",
"assets/assets/icons/iconosoporte-01.png": "53b02611c87998810fd33734312e761f",
"assets/assets/icons/ic_letras_blanco.png": "14a3c5d525229a584b2cacc398259f64",
"assets/assets/icons/lock.png": "712058c63227305c009a207bde11c3bc",
"assets/assets/icons/lock_a.png": "9016b48160b3b8427ca83ceb631ed6c5",
"assets/assets/icons/lock_r.png": "4faf26c8a3fcedc35bdee92a51550e11",
"assets/assets/icons/logo.png": "4575f99cb94dd5b34e31aaa8493d5227",
"assets/assets/icons/logoc_bbn.png": "c26df1c40414ee6b9e657c87ed1eccd7",
"assets/assets/icons/logoc_nbb.png": "6e78cb9c6b820facff2b43305a7080d4",
"assets/assets/icons/logoc_negro.png": "e86a305bdeec95853930d210f03c59cf",
"assets/assets/icons/logoc_rojo.png": "37cbb4a06df948db9f7bbff4422a08eb",
"assets/assets/icons/logoc_transparent.png": "82c904fe17c7be6a16528a07046437b7",
"assets/assets/icons/logoi.png": "7f395a141d6346e74e6ee730852d223c",
"assets/assets/icons/logoi_amarillo.png": "59bcdd0f52e1bab5a62b787d38b483db",
"assets/assets/icons/logoi_negro.png": "570909432d3fce0da732223cb3011ff3",
"assets/assets/icons/logoi_rojo.png": "1b907e1c13dbe5f54dceb60c1f899df7",
"assets/assets/icons/logol.png": "c021aef170d849b67f1f22f63e291dab",
"assets/assets/icons/logol_transparente.png": "a0189b87977678b27287d82b0ff187f7",
"assets/assets/icons/mail.png": "6b473bed301310bc664387fd838bf42e",
"assets/assets/icons/pregunta.png": "84981482e402ffd88d1912f14dafdaa7",
"assets/assets/icons/pregunta_a.png": "1cda3d4735d08a11d38077f2635fc11a",
"assets/assets/icons/pregunta_r.png": "9d4b8d5e82d247962d9ad23eea95748f",
"assets/assets/icons/regalo.png": "afee3c05dd2788833f172813a6450b92",
"assets/assets/icons/regalo_a.png": "e793b839ad7210ce11d09eb7a8a23b0d",
"assets/assets/icons/regalo_r.png": "1503eef9de35670a33fb80054f046108",
"assets/assets/icons/unlock.png": "92c8cf18006e8c8333d9f5057f7b9509",
"assets/assets/icons/unlock_a.png": "cdf465e56fe32976151ea0d1e7fe28b9",
"assets/assets/icons/unlock_r.png": "bec7e409295bf7672091ffbd8fe90c86",
"assets/assets/images/2x/Avatar.png": "0390675fbfad11cd7e933aa4db62668c",
"assets/assets/images/2x/Avatar_clean.png": "9c7264faf7cbeb0bebf77c7decd3a5a9",
"assets/assets/images/2x/Background.png": "5e97fab9dc2e1b86a88df131f5d7c7d1",
"assets/assets/images/2x/BackgroundAlerta.png": "f57afaad22ae47d7ffb01bf49de756b0",
"assets/assets/images/2x/BackgroundHome.png": "4f34873fd051fbab053decc02b2f85ab",
"assets/assets/images/2x/BackgroundPerfil.png": "066e90e1dcf2ed2270cf437d225af551",
"assets/assets/images/2x/Capa1.png": "e6b31aa723a5af5ccd858b1d776294bb",
"assets/assets/images/2x/capa11.jpg": "253ef36e95f4d3cd2a1e74ee2b6a99cb",
"assets/assets/images/2x/capa15.jpg": "9dc933863a2aa4ce2e1bbeff23b83f97",
"assets/assets/images/2x/capa19.jpg": "98474f42cefa9da6316db39d102abf1e",
"assets/assets/images/2x/Capa2.png": "055964970a2719a261f0542f127e8bcf",
"assets/assets/images/2x/capa3.jpg": "1ea69f5c081f1999299728ed222b0c54",
"assets/assets/images/2x/capa7.jpg": "52bf53b1d7e1d424fda7ea0ceb292b22",
"assets/assets/images/2x/IconBeneficios.png": "3b94ffef862c00c5e15972f9502fab0b",
"assets/assets/images/2x/IconBeneficiosDisabled.png": "baccc8d2292f50f593e1e99c1e9ab375",
"assets/assets/images/2x/IconBeneficiosOff.png": "8f43540a526e5ea1ef0c70491aff3a7b",
"assets/assets/images/2x/IconBolsillo.png": "24ea4f06cb149538cf95a03262f6dcc9",
"assets/assets/images/2x/IconBolsilloDisabled.png": "cfc4f2d1f60ceb3c54a154b85287f07c",
"assets/assets/images/2x/IconBolsilloOff.png": "fa8685766dd49a3e1fd34483e5dbe7d9",
"assets/assets/images/2x/IconHome.png": "ddd59c0dae820719c9857307fc3da01b",
"assets/assets/images/2x/IconHomeDisabled.png": "91d5c7ca057ee02b54a5cec02fbcc0d4",
"assets/assets/images/2x/IconHomeOff.png": "9d6aebdfbd71ffc728f381ee036ef5ca",
"assets/assets/images/2x/IconoBuscar.png": "0c391cd443de59caec28b72d7e1ea3e3",
"assets/assets/images/2x/IconoCentral.png": "9121f42db588c34c76f33e607357b215",
"assets/assets/images/2x/IconoPermiso.png": "0ea95c08684ce75f7d2642c2cc9e1ded",
"assets/assets/images/2x/IconoPregonear.png": "f8dfc180c6e5fd6e4a3612350c288871",
"assets/assets/images/2x/IconoVideo.png": "cc2a961688b2496391b66c9127b50ad9",
"assets/assets/images/2x/IconPregones.png": "944284063966979708d7ab5c10806305",
"assets/assets/images/2x/IconPregonesDisabled.png": "d511da706a7e805fcc24c3cba6f97c9f",
"assets/assets/images/2x/IconPregonesOff.png": "c645fb7aa0c12eb8a705d04e8eb3aac1",
"assets/assets/images/2x/Logo.png": "f11f692814ecfad2c06ce91935873805",
"assets/assets/images/2x/LogoRojo.png": "14c188555b3e3f6e1bfb62f25736b3ac",
"assets/assets/images/2x/vocero_blanco.png": "f77355d5060b52433b48a9dc5c9e9eee",
"assets/assets/images/2x/vocero_blanco_g.png": "5fe276e009ee79eda5ea10ae11ba70d6",
"assets/assets/images/3x/Avatar.png": "1d8a94f0c165c0b91a270b0c1188be6c",
"assets/assets/images/3x/Avatar_clean.png": "3d86be259adffd79d4742bbd9266a2f6",
"assets/assets/images/3x/Background.png": "e785f8cce40fcb50136b886ab2d8fcf3",
"assets/assets/images/3x/BackgroundAlerta.png": "a3e418ea391b2dc1a8bed7a2f5956363",
"assets/assets/images/3x/BackgroundHome.png": "78b8132608144e9cfca632bbb9eb0365",
"assets/assets/images/3x/BackgroundPerfil.png": "d6c99678b0e18df124d70944455ca892",
"assets/assets/images/3x/Capa1.png": "269be5a2f7201ea7d9cf8aaf3dbe9e1d",
"assets/assets/images/3x/capa11.jpg": "9877dcc68b75a1d558746f0572aeb0be",
"assets/assets/images/3x/capa15.jpg": "e7464f9c69a02bb31db8418fa99d891a",
"assets/assets/images/3x/capa19.jpg": "25c8ede91de02dcd832cc1b36c738d56",
"assets/assets/images/3x/Capa2.png": "5a1fd21f04140df59fc23ba07abd57bf",
"assets/assets/images/3x/capa3.jpg": "5acf4588ec06c45dafaffc1cf9a254ad",
"assets/assets/images/3x/capa7.jpg": "936a09b11c3860d288b487cabf1eab95",
"assets/assets/images/3x/IconBeneficios.png": "6e3fd98bba9a85da5e9a4e02c3868ecd",
"assets/assets/images/3x/IconBeneficiosDisabled.png": "4978df3f89e26f2e49220ae1f0a6e17f",
"assets/assets/images/3x/IconBeneficiosOff.png": "8564893f93c67a344f3326f8b558354d",
"assets/assets/images/3x/IconBolsillo.png": "5f1fa5ea7c338f5c9e4ecd41446c81c4",
"assets/assets/images/3x/IconBolsilloDisabled.png": "717263fabda4042923bfe0364c934fd4",
"assets/assets/images/3x/IconBolsilloOff.png": "ddd3d425cf757d5d6b9579cad6698214",
"assets/assets/images/3x/IconHome.png": "251c62286bd44a584731b5d4779b583a",
"assets/assets/images/3x/IconHomeDisabled.png": "21da613cce000b3e2ee7ae49281a09e1",
"assets/assets/images/3x/IconHomeOff.png": "e3e8654f50a54bd86ebdb53af9184302",
"assets/assets/images/3x/IconoBuscar.png": "ba4f200ffa8b383486bc24b1fd602b56",
"assets/assets/images/3x/IconoCentral.png": "c52643d42b6d8913537c1522b25914a5",
"assets/assets/images/3x/IconoPermiso.png": "34b26e7ec82ed6918ad9c632121c39fa",
"assets/assets/images/3x/IconoPregonear.png": "e36fb6f3ba7e3f82075cc5d63226f7f1",
"assets/assets/images/3x/IconoVideo.png": "7d6882a09a510f5bcafd01ed18f647d3",
"assets/assets/images/3x/IconPregones.png": "1533b215abb0a508e27b9fae71e0c09e",
"assets/assets/images/3x/IconPregonesDisabled.png": "6956e47953751ca5c9567ef7f252de04",
"assets/assets/images/3x/IconPregonesOff.png": "58902d6c92ed3163c6added9005718bf",
"assets/assets/images/3x/Logo.png": "77c42c4c2ab7d86390b78b50d06a77cb",
"assets/assets/images/3x/LogoRojo.png": "706e1515b4b9f2e0da68d5a2a5373207",
"assets/assets/images/3x/vocero_blanco.png": "3900fc5edac491a0d9e756847e1e2f29",
"assets/assets/images/3x/vocero_blanco_g.png": "590667901a7aedae103cd379eafe3654",
"assets/assets/images/Avatar.png": "73d712a372d05351c8e144eb50889aa6",
"assets/assets/images/Avatar_clean.png": "beca898fd49c4f562becc9aff82bf2e8",
"assets/assets/images/avatar_icono.png": "2e4563392852cd0f09cdc792ebd543c8",
"assets/assets/images/Background.png": "550568761437aa5bfed0a0a23efc2c48",
"assets/assets/images/BackgroundAlerta.png": "ab0f91fb64c2bec3c76d45a948b1c307",
"assets/assets/images/BackgroundHome.png": "9ab42da649ce0f8a288ffdbbeca8253d",
"assets/assets/images/BackgroundInterno.png": "6adfe589e37d7d243dcbdd7399d08b4e",
"assets/assets/images/BackgroundPerfil.png": "0b21d69794273eaa8b563d83fa843205",
"assets/assets/images/Capa1.png": "79e502cc9d8299d915679acfdd97e60f",
"assets/assets/images/capa11.jpg": "76c0b03e488e98c17fefcc3358869aac",
"assets/assets/images/capa15.jpg": "a41e1450031bc8e3cbe40c984561e7d7",
"assets/assets/images/capa19.jpg": "262e56053c622adcd331811f0b833ea7",
"assets/assets/images/Capa2.png": "63ff67305c0c2fd5abf516e2d72f3449",
"assets/assets/images/capa3.jpg": "dddcbe47b23c5a4450962580a7b91573",
"assets/assets/images/capa7.jpg": "d2a89238b5dfcdbbce29bc08cd20dce6",
"assets/assets/images/IconBeneficios.png": "f26bce1bd33ca1a0f508744d2aaf2e18",
"assets/assets/images/IconBeneficiosDisabled.png": "61b7bd257f1c56b64a22dbd02db57dd9",
"assets/assets/images/IconBeneficiosOff.png": "3c2cab4b1a39b4466f1b33ec47dbddb6",
"assets/assets/images/IconBolsillo.png": "34bf6e17e59a5539e6b9135536afaf31",
"assets/assets/images/IconBolsilloDisabled.png": "7adecb833b5ccd18b3e594cee5c32530",
"assets/assets/images/IconBolsilloOff.png": "e94cd78e6ce4a17fd79cc5491e9f7bb2",
"assets/assets/images/IconHome.png": "2fc36bcc193586ad101e035c6bbfde3c",
"assets/assets/images/IconHomeDisabled.png": "e7bee5c23a11b19c18fc8896b176974d",
"assets/assets/images/IconHomeOff.png": "2ba85a3b422b3acc9fb82de547d56f33",
"assets/assets/images/IconoBuscar.png": "187b64fabac27eb6c842a18883eb945e",
"assets/assets/images/IconoCentral.png": "2315406bc007c7139a9a9f16477d8675",
"assets/assets/images/IconoPermiso.png": "6cc09a323636b0057f8f0f50f0768b3d",
"assets/assets/images/IconoPregonear.png": "b200335b9cda98cbe898070655c86167",
"assets/assets/images/IconoVideo.png": "17d63068cd12c922c27d09ca3520a30d",
"assets/assets/images/IconPregones.png": "777580a8cee2d9faa9a5d8a1373d2838",
"assets/assets/images/IconPregonesDisabled.png": "b840a73aa9a6e7a6adde6d735e8ca909",
"assets/assets/images/IconPregonesOff.png": "de3aa496bd90018cdd4abbb4a1a67e36",
"assets/assets/images/Logo.png": "dadcab89f2bec64f03e0f8072d145517",
"assets/assets/images/LogoRojo.png": "c2899119c67c205e1a1161265e280f71",
"assets/assets/images/vocero_blanco.png": "e3d7f8a8495b666d66232b07de9b6ebe",
"assets/assets/images/vocero_blanco_g.png": "10c64b6856d2595004e79e28ad97b4aa",
"assets/assets/tflit/coco_ssd_mobilnet_v1_1.0.tflite": "efbf596715eec74b5d5cea0e9dff573b",
"assets/assets/tflit/coco_ssd_mobilnet_v1_1.0.txt": "631c1fba8f6d9595270e5b46757c64c6",
"assets/assets/tflit/posenet_mv1_075_float_from_checkpoints.tflite": "e0c83d992292731a3f2dfe387d1470a6",
"assets/FontManifest.json": "d3146c2d1a8622b8744a3a91a55bbc43",
"assets/fonts/MaterialIcons-Regular.otf": "a68d2a28c526b3b070aefca4bac93d25",
"assets/NOTICES": "f47423c77dec83c1ca7e6960056222c6",
"assets/packages/cupertino_icons/assets/CupertinoIcons.ttf": "115e937bb829a890521f72d2e664b632",
"assets/packages/flutter_inappwebview/t_rex_runner/t-rex.css": "5a8d0222407e388155d7d1395a75d5b9",
"assets/packages/flutter_inappwebview/t_rex_runner/t-rex.html": "16911fcc170c8af1c5457940bd0bf055",
"assets/packages/youtube_player_flutter/assets/speedometer.webp": "50448630e948b5b3998ae5a5d112622b",
"icons/apple-touch-icon.png": "b1690bda420a9d1085af77c1c906774c",
"icons/favicon-16x16.png": "10f20699804b13d22a6c532e9dfe10f6",
"icons/favicon-32x32.png": "ca400ca7c7c9a0f40d1df1c6418f4bd9",
"icons/favicon.ico": "d20d62f96f28c59923ee91166fc341a3",
"icons/Icon-192.png": "383b5027606c927b3fc71c85e2d5b323",
"icons/Icon-512.png": "cf51556afaf1c47b97d73602e299c57a",
"icons/safari-pinned-tab.svg": "23577c7b660bef2609ce4c17398d3e7b",
"index.html": "890061534d088d122d75652c2c240ea1",
"/": "890061534d088d122d75652c2c240ea1",
"main.dart.js": "a03aa81eb54456a956deb1b23be3cf65",
"manifest.json": "6066225ca3d425ec3415dca964a24eac"
};

// The application shell files that are downloaded before a service worker can
// start.
const CORE = [
  "/",
"main.dart.js",
"index.html",
"assets/NOTICES",
"assets/AssetManifest.json",
"assets/FontManifest.json"];

// During install, the TEMP cache is populated with the application shell files.
self.addEventListener("install", (event) => {
  return event.waitUntil(
    caches.open(TEMP).then((cache) => {
      // Provide a 'reload' param to ensure the latest version is downloaded.
      return cache.addAll(CORE.map((value) => new Request(value, {'cache': 'reload'})));
    })
  );
});

// During activate, the cache is populated with the temp files downloaded in
// install. If this service worker is upgrading from one with a saved
// MANIFEST, then use this to retain unchanged resource files.
self.addEventListener("activate", function(event) {
  return event.waitUntil(async function() {
    try {
      var contentCache = await caches.open(CACHE_NAME);
      var tempCache = await caches.open(TEMP);
      var manifestCache = await caches.open(MANIFEST);
      var manifest = await manifestCache.match('manifest');

      // When there is no prior manifest, clear the entire cache.
      if (!manifest) {
        await caches.delete(CACHE_NAME);
        contentCache = await caches.open(CACHE_NAME);
        for (var request of await tempCache.keys()) {
          var response = await tempCache.match(request);
          await contentCache.put(request, response);
        }
        await caches.delete(TEMP);
        // Save the manifest to make future upgrades efficient.
        await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
        return;
      }

      var oldManifest = await manifest.json();
      var origin = self.location.origin;
      for (var request of await contentCache.keys()) {
        var key = request.url.substring(origin.length + 1);
        if (key == "") {
          key = "/";
        }
        // If a resource from the old manifest is not in the new cache, or if
        // the MD5 sum has changed, delete it. Otherwise the resource is left
        // in the cache and can be reused by the new service worker.
        if (!RESOURCES[key] || RESOURCES[key] != oldManifest[key]) {
          await contentCache.delete(request);
        }
      }
      // Populate the cache with the app shell TEMP files, potentially overwriting
      // cache files preserved above.
      for (var request of await tempCache.keys()) {
        var response = await tempCache.match(request);
        await contentCache.put(request, response);
      }
      await caches.delete(TEMP);
      // Save the manifest to make future upgrades efficient.
      await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
      return;
    } catch (err) {
      // On an unhandled exception the state of the cache cannot be guaranteed.
      console.error('Failed to upgrade service worker: ' + err);
      await caches.delete(CACHE_NAME);
      await caches.delete(TEMP);
      await caches.delete(MANIFEST);
    }
  }());
});

// The fetch handler redirects requests for RESOURCE files to the service
// worker cache.
self.addEventListener("fetch", (event) => {
  var origin = self.location.origin;
  var key = event.request.url.substring(origin.length + 1);
  // Redirect URLs to the index.html
  if (event.request.url == origin || event.request.url.startsWith(origin + '/#')) {
    key = '/';
  }
  // If the URL is not the RESOURCE list, skip the cache.
  if (!RESOURCES[key]) {
    return event.respondWith(fetch(event.request));
  }
  event.respondWith(caches.open(CACHE_NAME)
    .then((cache) =>  {
      return cache.match(event.request).then((response) => {
        // Either respond with the cached resource, or perform a fetch and
        // lazily populate the cache. Ensure the resources are not cached
        // by the browser for longer than the service worker expects.
        var modifiedRequest = new Request(event.request, {'cache': 'reload'});
        return response || fetch(modifiedRequest).then((response) => {
          cache.put(event.request, response.clone());
          return response;
        });
      })
    })
  );
});

self.addEventListener('message', (event) => {
  // SkipWaiting can be used to immediately activate a waiting service worker.
  // This will also require a page refresh triggered by the main worker.
  if (event.data === 'skipWaiting') {
    return self.skipWaiting();
  }

  if (event.message === 'downloadOffline') {
    downloadOffline();
  }
});

// Download offline will check the RESOURCES for all files not in the cache
// and populate them.
async function downloadOffline() {
  var resources = [];
  var contentCache = await caches.open(CACHE_NAME);
  var currentContent = {};
  for (var request of await contentCache.keys()) {
    var key = request.url.substring(origin.length + 1);
    if (key == "") {
      key = "/";
    }
    currentContent[key] = true;
  }
  for (var resourceKey in Object.keys(RESOURCES)) {
    if (!currentContent[resourceKey]) {
      resources.push(resourceKey);
    }
  }
  return contentCache.addAll(resources);
}
